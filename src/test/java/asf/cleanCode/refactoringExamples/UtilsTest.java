package asf.cleanCode.refactoringExamples;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class UtilsTest {

    @Test
    public void tiempoEnSegundosPara10DiasDevuelveValorCorrecto() {
        assertEquals(Utils.tiempoEnSegundos(10), 864000 );
    }

    @Test
    public void generarIdAPartirDeFechaConMesDeUnDigitoDevuelveValorCorrecto(){
        LocalDateTime localDateTime = LocalDateTime.of(2019, 9, 23, 18, 0, 0);
        assertEquals(Utils.generarIdAPartirDeFecha(localDateTime), "ASF2019090");
    }

    @Test
    public void generarIdAPartirDeFechaConMesDosDigitosDevuelveValorCorrecto(){
        LocalDateTime localDateTime = LocalDateTime.of(2019, 10, 23, 18, 0, 27);
        assertEquals(Utils.generarIdAPartirDeFecha(localDateTime), "ASF20191027");
    }
}