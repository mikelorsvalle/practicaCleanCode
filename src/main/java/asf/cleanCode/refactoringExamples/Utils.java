package asf.cleanCode.refactoringExamples;

import java.time.LocalDateTime;

public class Utils {

    public static int tiempoEnSegundos(int dias) {
        return dias * (24 * 60 * 60);
    }

    public static String generarIdAPartirDeFecha(LocalDateTime localDateTime) {
        return "ASF" +
                localDateTime.getYear() +
                (localDateTime.getMonthValue()<=9 ?
                        "0" + localDateTime.getMonthValue() :
                        localDateTime.getMonthValue()
                        ) +
                localDateTime.getSecond();

    }
}
