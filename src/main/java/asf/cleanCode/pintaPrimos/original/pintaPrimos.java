package asf.cleanCode.pintaPrimos.original;

public class pintaPrimos {
  public static void pinta() {
    final int max = 1000;
    final int lin = 50;
    final int col = 4;
    final int kl = 10;
    final int ordmax = 30;
    int array1[] = new int[max + 1];
    int NUMERODEPAGINA;
    int PAGEOFFSET;
    int ROWOFFSET;
    int i;
    int j;
    int k;
    boolean primo;
    int ord;
    int cuadrado;
    int N;
    int MULT[] = new int[ordmax + 1];

    j = 1;
    k = 1;
    array1[1] = 2;
    ord = 2;
    cuadrado = 9;

    while (k < max) {
      do {
        j = j + 2;
        if (j == cuadrado) {
          ord = ord + 1;
          cuadrado = array1[ord] * array1[ord];
          MULT[ord - 1] = j;
        }
        N = 2;
        primo = true;
        while (N < ord && primo) {
          while (MULT[N] < j)
            MULT[N] = MULT[N] + array1[N] + array1[N];
          if (MULT[N] == j)
            primo = false;
          N = N + 1;
        }
      } while (!primo);
      k = k + 1;
      array1[k] = j;
    }
    {
      NUMERODEPAGINA = 1;
      PAGEOFFSET = 1;
      while (PAGEOFFSET <= max) {
        System.out.println("Los primeros " + max +
                             " Números primos --- Página " + NUMERODEPAGINA);
        System.out.println("");
        for (ROWOFFSET = PAGEOFFSET; ROWOFFSET < PAGEOFFSET + lin; ROWOFFSET++){
          for (i = 0; i < col;i++)
            if (ROWOFFSET + i * lin <= max)
              System.out.format("%10d", array1[ROWOFFSET + i * lin]);
          System.out.println("");
        }
        System.out.println("\f");
        NUMERODEPAGINA = NUMERODEPAGINA + 1;
        PAGEOFFSET = PAGEOFFSET + lin * col;
      }
    }
  }
}