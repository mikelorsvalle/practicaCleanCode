Exercise 1 - Ugly code
In this exercise you will be cleaning up and refactoring ugly, obfuscated code.

We are going to assume you have IntelliJ Community Edition installed.

1. Dev environment
Before you begin: Please make sure that you have the following installed:

IntelliJ Community Edition: IntelliJ Community Edition
Java dev kit: Java SE Development Kit
Open pom.xml in IntelliJ by selecting Import project from the Welcome Screen.
2. Assignment
The code does some math-y things...
Analyze and understand the code.
Write tests that verify your presumptions (See the All about testing code workshop exercises if you have forgotten how to write tests)
Refactor the code ensure the tests are still passing.